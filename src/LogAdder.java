import java.io.IOException;
import java.util.logging.Level;

public class LogAdder {
    static MyLogger myLogger;

    static {
        try {
            myLogger = new MyLogger("logs.txt");
            myLogger.logger.setLevel(Level.ALL);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static public void addLogInfo(String log) {
        myLogger.logger.info(log);
    }

    static public void addLogWarning(String log) {
        myLogger.logger.warning(log);
    }

    static public void addLogServer(String log) {
        myLogger.logger.severe(log);
    }
}
