import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class Interface extends JFrame implements ActionListener {
    Vector<String> assetsV = new Vector<String>(0);
    JComboBox<String> assets;
    int amount;
    int boughtCompanyNumber;
    double assetPrice;
    JTextField amountTextField;
    JTextField priceTextField;
    JButton submitButton;
    int userNum;

    String operation = "none";

    Interface(int userNumber) {
        userNum = userNumber;
        fillInAssets();

        assets = new JComboBox<>(assetsV);
        assets.addActionListener(this);

        JLabel massageAmount = new JLabel("How much you want to " + operation);
        massageAmount.setForeground(Color.WHITE);
        massageAmount.setFont(new Font("MV Boli", Font.PLAIN, 17));

        amountTextField = new JTextField();
        amountTextField.setPreferredSize(new Dimension(250, 40));
        amountTextField.setToolTipText("write amount");

        JLabel massagePrice = new JLabel("What price in $ (write just number)");
        massagePrice.setForeground(Color.WHITE);
        massagePrice.setFont(new Font("MV Boli", Font.PLAIN, 17));

        priceTextField = new JTextField();
        priceTextField.setPreferredSize(new Dimension(250, 40));
        priceTextField.setToolTipText("write price in $ (write just number)");

        submitButton = new JButton("Submit");
        submitButton.addActionListener(this);

        this.setTitle(operation + " Window");
        this.getContentPane().setBackground(Color.BLACK);
        this.add(assets);
        this.add(massageAmount);
        this.add(amountTextField);
        this.add(massagePrice);
        this.add(priceTextField);
        this.add(submitButton);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new FlowLayout());
        this.setBounds(0, 0, 330, 300);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == assets) {
            boughtCompanyNumber = assets.getSelectedIndex();
        } else if (e.getSource() == submitButton) {
            if (boughtCompanyNumber == -1) {
                PopUpWarningMassage info = new PopUpWarningMassage("Please choose company u want to " + operation, "CHOOSE COMPANY");
                return;
            }
            try {
                amount = Integer.parseInt(amountTextField.getText());
                assetPrice = Integer.parseInt(priceTextField.getText());
                if (assetPrice > 1000 || assetPrice < 1) {
                    PopUpWarningMassage info = new PopUpWarningMassage("Wrong input price, write price between " +
                            AssetManager.LOWEST_ASSET_PRICE + " and " + AssetManager.HIGHEST_ASSET_PRICE, "CHANGE PRICE");
                    return;
                }
                performOperation();
            } catch (Exception error) {
                PopUpWarningMassage info = new PopUpWarningMassage("Incorrect input, Please write only numbers in text fields", "WTRITE AMOUNT");
            }
        }
    }

    private void fillInAssets() {
    }

    private void performOperation() {
    }
}
