import java.util.Vector;

public class Stock {
    static AssetManager stockExchange;
    static Vector<User> UserList = new Vector<User>(0);
    static int numberOfUsers = 0;

    public static void main(String[] args) {
        Stock stock = new Stock();
        stock.initiateNewStock();
        stock.stockManage();
    }

    void initiateNewStock() {
        LogAdder.addLogInfo("New Stock created");
        stockExchange = new AssetManager();
    }

    void stockManage() {
        runInterfaces();
        runStock();
    }

    void runInterfaces() {
        Runnable UserInterface = new Runnable() {
            public void run() {
                StockInterface stock = new StockInterface();
            }
        };
        Thread thread1 = new Thread(UserInterface);
        thread1.start();
    }

    void runStock() {
        Runnable UserInterface2 = new Runnable() {
            public void run() {
                while (true) {
                    Stock.stockExchange.runStock();
                }
            }
        };
        Thread thread2 = new Thread(UserInterface2);
        thread2.start();
    }
}
