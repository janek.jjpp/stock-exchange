public class Order {
    int userNumber;
    double price;
    String assetName;
    int amount;
    User user;

    Order(String companyName, double orderedPrice, int orderedAmount, User orderingUser) {
        user = orderingUser;
        price = orderedPrice;
        assetName = companyName;
        amount = orderedAmount;
    }
}
