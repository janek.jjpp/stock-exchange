import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserInfoPanel extends JPanel implements ActionListener {
    private final static int PANEL_WIDTH = 600;
    private final static int PANEL_HEIGHT = 300;
    private final static int REFRESH = 1000;

    Timer timer;
    JLabel assetsMassages = new JLabel("Loading assets..", SwingConstants.CENTER);

    int userNum;

    UserInfoPanel(int userNumber) {
        userNum = userNumber;
        assetsMassages.setForeground(Color.WHITE);
        assetsMassages.setFont(new Font("MV Boli", Font.PLAIN, 16));

        this.add(assetsMassages, BorderLayout.WEST);
        this.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        this.setBackground(Color.BLACK);

        timer = new Timer(REFRESH, this);
        timer.start();
        setLayout(new GridLayout(0, 1));
    }

    public void actionPerformed(ActionEvent e) {
        String s = "<html> User " + Stock.UserList.get(userNum).userName + " : deposit " + Stock.UserList.get(userNum).userDeposit + "$<br/>";
        s += "Owned Actions : <br/>";
        for (int i = 0; i < Stock.UserList.get(userNum).numberOfUserAssets; i++) {
            s += ("Company : " + Stock.UserList.get(userNum).userWallet.get(i).name + ", buy : " + String.format("%.2f", AssetManager.findPrice(Stock.UserList.get(userNum).userWallet.get(i).name)) +
                    "$, sell " + String.format("%.2f", AssetManager.findPrice(Stock.UserList.get(userNum).userWallet.get(i).name) *
                    (1 - Asset.COMMISSION)) + "$, amount " + Stock.UserList.get(userNum).userWallet.get(i).amount + "<br/>");
        }
        s += "</html>";
        assetsMassages.setText(s);
        repaint();
    }
}
