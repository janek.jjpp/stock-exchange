import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class SellInterface extends JFrame implements ActionListener {
    Vector<String> yourAssets = new Vector<String>(0);
    JComboBox<String> assets;
    int amountToSell;
    int sellCompanyNumber;
    JTextField amountTextField;
    JTextField priceTextField;
    JButton submitButton;
    int userNum;
    double assetPrice;

    SellInterface(int userNumber) {
        userNum = userNumber;
        if (Stock.UserList.get(userNum).numberOfUserAssets == 0) {
            PopUpWarningMassage info = new PopUpWarningMassage("You dont have any actions", "No actions");
            return;
        }
        fillInAssets();

        assets = new JComboBox<>(yourAssets);
        assets.addActionListener(this);

        JLabel massage = new JLabel("How much you want to Sell");
        massage.setForeground(Color.WHITE);
        massage.setFont(new Font("MV Boli", Font.PLAIN, 17));

        amountTextField = new JTextField();
        amountTextField.setPreferredSize(new Dimension(250, 40));
        amountTextField.setToolTipText("write amount");

        JLabel massagePrice = new JLabel("What price in $ (write just number)");
        massagePrice.setForeground(Color.WHITE);
        massagePrice.setFont(new Font("MV Boli", Font.PLAIN, 17));

        priceTextField = new JTextField();
        priceTextField.setPreferredSize(new Dimension(250, 40));
        priceTextField.setToolTipText("write price in $ (write just number)");

        submitButton = new JButton("Submit");
        submitButton.addActionListener(this);

        this.setTitle("Sell Window");
        this.getContentPane().setBackground(Color.BLACK);
        this.add(assets);
        this.add(massage);
        this.add(amountTextField);
        this.add(massagePrice);
        this.add(priceTextField);
        this.add(submitButton);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new FlowLayout());
        this.setBounds(0, 0, 330, 300);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == assets) {
            sellCompanyNumber = assets.getSelectedIndex();
        } else if (e.getSource() == submitButton) {
            if (sellCompanyNumber == -1) {
                PopUpWarningMassage info = new PopUpWarningMassage("Please choose company u want to sell", "CHOOSE COMPANY");
                return;
            }
            try {
                amountToSell = Integer.parseInt(amountTextField.getText());
                assetPrice = Integer.parseInt(priceTextField.getText());
                if (assetPrice > 1000 || assetPrice < 1) {
                    PopUpWarningMassage info = new PopUpWarningMassage("Wrong input price, write price between " +
                            AssetManager.LOWEST_ASSET_PRICE + " and " + AssetManager.HIGHEST_ASSET_PRICE, "CHANGE PRICE");
                    return;
                }
                performOperation();
            } catch (Exception error) {
                PopUpWarningMassage info = new PopUpWarningMassage("Please write amount (write just number)", "WRITE AMOUNT");
            }
        }
    }

    private void fillInAssets() {
        for (int numerator = 0; numerator < Stock.UserList.get(userNum).numberOfUserAssets; numerator++) {
            yourAssets.add(Stock.UserList.get(userNum).userWallet.get(numerator).name + " " +
                    String.format("%.2f", AssetManager.findPrice(Stock.UserList.get(userNum).userWallet.get(numerator).name) *
                            (1 - Asset.COMMISSION)) + "$ - amount " + Stock.UserList.get(userNum).userWallet.get(numerator).amount);
        }
    }

    private void performOperation() {
        sellCompanyNumber = assets.getSelectedIndex();
        String sellingAssetName = Stock.UserList.get(userNum).userWallet.get(sellCompanyNumber).name;
        if (Stock.UserList.get(userNum).userWallet.get(sellCompanyNumber).amount >= amountToSell) {
            PopUpInfoMassage info = new PopUpInfoMassage("User " + Stock.UserList.get(userNum).userName + " placed sell order for " + amountToSell +
                    " actions of company : " + sellingAssetName + Stock.UserList.get(userNum).userWallet.get(sellCompanyNumber).price + "$", "Succeed");
            AssetManager.ordersQueSell.add(new Order(Stock.UserList.get(userNum).userWallet.get(sellCompanyNumber).name,
                    assetPrice, amountToSell, Stock.UserList.get(userNum)));
            assets.removeAllItems();
            fillInAssets();
        } else {
            PopUpWarningMassage info = new PopUpWarningMassage("You dont have enough actions to perform this " +
                    "operation (close and open sell window to see current wallet)", "Not enough actions");
        }
    }
}
