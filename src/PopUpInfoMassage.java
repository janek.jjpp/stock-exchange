import javax.swing.*;

public class PopUpInfoMassage {
    PopUpInfoMassage(String massage, String title) {
        JOptionPane.showMessageDialog(null, massage, title, JOptionPane.INFORMATION_MESSAGE);
        LogAdder.addLogInfo(massage);
    }
}
