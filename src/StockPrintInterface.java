import javax.swing.*;

public class StockPrintInterface extends JFrame {
    PrintStockPricesPanel panel;

    StockPrintInterface() {
        this.setTitle("Stock Exchange");
        panel = new PrintStockPricesPanel();
        this.add(panel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
