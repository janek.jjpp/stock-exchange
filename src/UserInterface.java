import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class UserInterface implements ActionListener {
    private final static int FRAME_WIDTH = 650;
    private final static int FRAME_HEIGHT = 700;
    private final static int BUTTON_WIDTH = 100;
    private final static int BUTTON_HEIGHT = 50;
    private final static int BUTTON_VERTICAL_DISTANCE = 150;
    private final static int BUTTON_HORIZONTAL_SPACING = 40;
    private final static int LABEL_X = 0;
    private final static int LABEL_Y = 0;
    private final static int LABEL_WIDTH = 600;
    private final static int LABEL_HEIGHT = 300;


    Vector<User> stockUsers = new Vector<User>(0);
    JFrame frame = new JFrame();
    JLabel label = new JLabel();
    JButton buyButton = new JButton("Buy");
    JButton sellButton = new JButton("Sell");
    UserInfoPanel userInfoPanel;
    int thisUserNumber = -1;

    UserInterface() {
        String name;
        String deposit;
        int depo;
        try {
            name = JOptionPane.showInputDialog("Write user name");
            deposit = JOptionPane.showInputDialog("Write user deposit");
            depo = Integer.parseInt(deposit);
            createNewUser(name, depo);
        } catch (Exception e) {
            PopUpWarningMassage info = new PopUpWarningMassage("Write correct values in text fields", "Incorrect input");
            return;
        }

        userInfoPanel = new UserInfoPanel(thisUserNumber);
        stockUsers.add(new User(name, depo));

        buyButton.setBounds(BUTTON_VERTICAL_DISTANCE, BUTTON_HORIZONTAL_SPACING, BUTTON_WIDTH, BUTTON_HEIGHT);
        buyButton.setFocusable(false);
        buyButton.addActionListener(this);

        sellButton.setBounds(buyButton.getX() + BUTTON_VERTICAL_DISTANCE, BUTTON_HORIZONTAL_SPACING, BUTTON_WIDTH, BUTTON_HEIGHT);
        sellButton.setFocusable(false);
        sellButton.addActionListener(this);

        label.setHorizontalTextPosition(JLabel.CENTER);
        label.add(sellButton);
        label.add(buyButton);
        label.setBounds(LABEL_X, LABEL_Y, LABEL_WIDTH, LABEL_HEIGHT);
        label.setVerticalAlignment(JLabel.BOTTOM);
        label.setHorizontalAlignment(JLabel.CENTER);

        frame.setTitle("User " + name + " window");
        frame.setLocationRelativeTo(null);
        frame.add(userInfoPanel);
        frame.add(label);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setLayout(new GridLayout(0, 1));
        frame.setVisible(true);
        frame.getContentPane().setBackground(Color.BLACK);

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buyButton) {
            BuyInterface assetsList = new BuyInterface(thisUserNumber);
        } else if (e.getSource() == sellButton) {
            SellInterface PrintStock = new SellInterface(thisUserNumber);
        }
    }

    private void createNewUser(String name, int deposit) {
        LogAdder.addLogInfo("New User " + name + " with deposit " + deposit + "$");
        Stock.UserList.add(new User(name, deposit));
        thisUserNumber = Stock.numberOfUsers;
        Stock.numberOfUsers++;
    }
}
