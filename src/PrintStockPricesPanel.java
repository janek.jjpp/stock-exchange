import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PrintStockPricesPanel extends JPanel implements ActionListener {
    private final static int PANEL_WIDTH = 500;
    private final static int PANEL_HEIGHT = 300;
    private final static int REFRESH = 1000;

    Timer timer;
    Asset[] assets = AssetManager.stockAssets;
    int assetNumber = AssetManager.numberOfAssets;
    JLabel assetsMassages = new JLabel("Loading assets..", SwingConstants.CENTER);

    PrintStockPricesPanel() {
        assetsMassages.setForeground(Color.WHITE);
        assetsMassages.setFont(new Font("Serif ", Font.PLAIN, 14));

        this.add(assetsMassages, BorderLayout.WEST);
        this.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        this.setBackground(Color.BLACK);

        timer = new Timer(REFRESH, this);
        timer.start();
        setLayout(new GridLayout(0, 1));
    }

    public void actionPerformed(ActionEvent e) {
        String s = "<html>";
        for (int i = 0; i < assetNumber; i++) {
            s += ("Company : " + assets[i].name + ", buy : " + String.format("%.2f", assets[i].price) +
                    "$, sell " + String.format("%.2f", assets[i].price * (1 - Asset.COMMISSION)) + "$, amount " + assets[i].amount + "<br/>");
        }
        s += "</html>";
        assetsMassages.setText(s);
        repaint();
    }
}
