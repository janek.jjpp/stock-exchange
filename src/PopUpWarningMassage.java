import javax.swing.*;

public class PopUpWarningMassage {
    PopUpWarningMassage(String massage, String title) {
        JOptionPane.showMessageDialog(null, massage, title, JOptionPane.ERROR_MESSAGE);
        LogAdder.addLogWarning(massage);
    }
}
