import java.util.Vector;

public class User {
    String userName;
    int userDeposit;
    Vector<Asset> userWallet = new Vector<Asset>(1);
    int numberOfUserAssets;

    User(String name, int deposit) {
        numberOfUserAssets = 0;
        userDeposit = deposit;
        userName = name;
    }

    void addAsset(String name, int amount) {
        for (Asset userAsset : userWallet) {
            if (userAsset.name.equals(name)) {
                userAsset.amount += amount;
                return;
            }
        }
        numberOfUserAssets++;
        Asset newUser = new Asset(amount, name);
        userWallet.add(newUser);
    }

    void removeAsset(String name, int amount) {
        for (Asset userAsset : userWallet) {
            if (userAsset.name.equals(name)) {
                if (userAsset.amount - amount == 0) {
                    userWallet.removeElement(userAsset);
                    numberOfUserAssets--;
                }
                userAsset.amount -= amount;
                return;
            }
        }
    }
}
