public class Asset {
    final static double COMMISSION = 0.01;

    Asset(int stockAmount, String companyName) {
        name = companyName;
        amount = stockAmount;
    }

    Asset() {
    }

    public String name;
    public double price;
    public int amount;

    void printAsset() {
        System.out.println("Name : " + name + ", buy : " + String.format("%.2f", price) + "$, sell " + String.format("%.2f", price * (1 - COMMISSION)) + "$, amount " + amount);
    }
}
