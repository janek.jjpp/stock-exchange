import java.util.Random;
import java.util.Vector;
import java.lang.Math;

public class AssetManager {
    private final static String[] COMPANY_NAMES =
            {"Firsts", "Seconds", "Thirds", "Fourths", "Fifths", "Sixth", "Sevenths", "Eighths", "Ninths",
                    "Tenths", "Elevenths", "Twelfths", "Thirteenths", "Fourteenths", "Fifteenths"};
    private final boolean[] companyAlreadyInStock =
            {false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,};
    private final static int UPPER_BOUND_ASSET_NUMBER = 11;
    private final static int LOWER_BOUND_ASSET_NUMBER = 5;
    final static int LOWEST_ASSET_PRICE = 1;
    final static int HIGHEST_ASSET_PRICE = 1000;
    private final static int ASSETS_MAX_AMOUNT = 1000;
    private final static int AUTO_PRICE_CHANGE_PERIOD = 1000; // price changes every 5 loop rounds
    final static double BUY_SELL_PRICE_MULTIPLAYER = 0.1;
    final static double PRICE_RATIO_CHANGER = 5; // higher number result bigger change
    static Asset[] stockAssets;
    static Vector<Order> ordersQueBuy = new Vector<>(0);
    static Vector<Order> ordersQueSell = new Vector<>(0);
    static int numberOfAssets;
    private int loopRuns = 0;

    AssetManager() {
        numberOfAssets = getRandom(UPPER_BOUND_ASSET_NUMBER, LOWER_BOUND_ASSET_NUMBER);
        stockAssets = new Asset[numberOfAssets];
        for (int iterator = 0; iterator < numberOfAssets; iterator++) {
            stockAssets[iterator] = new Asset();
            stockAssets[iterator].name = getRandomName();
            stockAssets[iterator].price = getRandom(HIGHEST_ASSET_PRICE, LOWEST_ASSET_PRICE);
            stockAssets[iterator].amount = ASSETS_MAX_AMOUNT;
        }
    }

    static public double findPrice(String assetName) {
        for (int iterator = 0; iterator < numberOfAssets; iterator++) {
            if (stockAssets[iterator].name.equals(assetName))
                return stockAssets[iterator].price;
        }
        return 0.0;
    }

    static public void changePrice(Asset assetToChange, int operationAmount, int sign) {
        double priceChangeSum = (BUY_SELL_PRICE_MULTIPLAYER * operationAmount * sign) +
                Math.pow(BUY_SELL_PRICE_MULTIPLAYER, PRICE_RATIO_CHANGER) * operationAmount * sign *
                        (ASSETS_MAX_AMOUNT - assetToChange.amount) * assetToChange.price;
        if (assetToChange.price + priceChangeSum > HIGHEST_ASSET_PRICE)
            assetToChange.price = HIGHEST_ASSET_PRICE;
        else if (assetToChange.price + priceChangeSum < LOWEST_ASSET_PRICE)
            assetToChange.price = LOWEST_ASSET_PRICE;
        else
            assetToChange.price += priceChangeSum;
    }

    int getRandom(int upperBound, int lowerBound) {
        Random rand = new Random();
        return rand.nextInt(upperBound - lowerBound) + lowerBound;
    }

    String getRandomName() {
        int companyNameNumber = getRandom(15, 0);
        while (companyAlreadyInStock[companyNameNumber]) {
            companyNameNumber = getRandom(15, 0);
        }
        companyAlreadyInStock[companyNameNumber] = true;
        return COMPANY_NAMES[companyNameNumber];
    }

    void printStock() {
        System.out.println("Currently available Assets with prices:");
        for (Asset stockAsset : stockAssets) {
            stockAsset.printAsset();
        }
    }

    void runStock() {
        try {
            while (true) {
                executeOrderedBuys();
                executeOrderedSells();
                Thread.sleep(AUTO_PRICE_CHANGE_PERIOD);
                loopRuns++;
                if (loopRuns == 5) {
                    autoPriceChange();
                    loopRuns = 0;
                }
            }
        } catch (InterruptedException ex) {
            System.out.println("Something went wrong.. thread interrupted");
        }
    }

    void executeOrderedBuys() {
        for (int numerator = 0; numerator < ordersQueBuy.size(); numerator++) {
            for (int numerator2 = 0; numerator2 < stockAssets.length; numerator2++) {
                if (ordersQueBuy.get(numerator).assetName.equals(stockAssets[numerator2].name) && stockAssets[numerator2].price <= ordersQueBuy.get(numerator).price) {
                    int tempAmount;
                    if (stockAssets[numerator2].amount < ordersQueBuy.get(numerator).amount) {
                        tempAmount = ordersQueBuy.get(numerator).amount - stockAssets[numerator2].amount;
                        ordersQueBuy.get(numerator).amount = stockAssets[numerator2].amount;
                    } else {
                        tempAmount = 0;
                    }
                    stockAssets[numerator2].amount -= ordersQueBuy.get(numerator).amount;
                    ordersQueBuy.get(numerator).user.userDeposit -= AssetManager.stockAssets[numerator2].price * ordersQueBuy.get(numerator).amount;
                    ordersQueBuy.get(numerator).user.addAsset(ordersQueBuy.get(numerator).assetName, ordersQueBuy.get(numerator).amount);
                    PopUpInfoMassage info = new PopUpInfoMassage("User " + Stock.UserList.get(ordersQueBuy.get(numerator).userNumber).userName
                            + " Bought " + ordersQueBuy.get(numerator).assetName + " amount " + ordersQueBuy.get(numerator).amount +
                            " with price " + String.format("%.2f", stockAssets[numerator2].price) + "$", "Succeed");
                    changePrice(AssetManager.stockAssets[numerator2], ordersQueBuy.get(numerator).amount, 1);
                    if (tempAmount == 0) {
                        ordersQueBuy.removeElement(ordersQueBuy.get(numerator));
                    } else {
                        ordersQueBuy.get(numerator).amount = tempAmount;
                    }
                    break;
                }
            }
        }
    }

    void executeOrderedSells() {
        for (int numerator = 0; numerator < ordersQueSell.size(); numerator++) {
            for (int numerator2 = 0; numerator2 < stockAssets.length; numerator2++) {
                if (ordersQueSell.get(numerator).assetName.equals(stockAssets[numerator2].name) && stockAssets[numerator2].price >= ordersQueSell.get(numerator).price) {
                    stockAssets[numerator2].amount += ordersQueSell.get(numerator).amount;
                    ordersQueSell.get(numerator).user.userDeposit += AssetManager.stockAssets[numerator2].price * ordersQueSell.get(numerator).amount;
                    ordersQueSell.get(numerator).user.removeAsset(ordersQueSell.get(numerator).assetName, ordersQueSell.get(numerator).amount);
                    PopUpInfoMassage info = new PopUpInfoMassage("User " + Stock.UserList.get(ordersQueSell.get(numerator).userNumber).userName
                            + " Sold " + ordersQueSell.get(numerator).assetName + " amount " + ordersQueSell.get(numerator).amount +
                            " with price " + String.format("%.2f", stockAssets[numerator2].price) + "$", "Succeed");
                    changePrice(AssetManager.stockAssets[numerator2], ordersQueSell.get(numerator).amount, -1);
                    ordersQueSell.removeElement(ordersQueSell.get(numerator));
                    break;
                }
            }
        }
    }

    void autoPriceChange() {
        int random;
        int sign;
        for (Asset stockAsset : stockAssets) {
            random = getRandom(4, 1);
            sign = getRandom(2, 0);
            if (sign == 1) {
                if (stockAsset.price + random < HIGHEST_ASSET_PRICE)
                    stockAsset.price += random;
            } else {
                if (stockAsset.price - random > LOWEST_ASSET_PRICE)
                    stockAsset.price -= random;
            }
        }
    }

}
