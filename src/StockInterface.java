import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StockInterface implements ActionListener {

    private final static int FRAME_WIDTH = 500;
    private final static int FRAME_HEIGHT = 300;
    private final static int BUTTON_WIDTH = 150;
    private final static int BUTTON_HEIGHT = 50;
    private final static int BUTTON_VERTICAL_DISTANCE = 170;
    private final static int BUTTON_HORIZONTAL_SPACING = 60;

    JFrame frame = new JFrame("Admin");
    JLabel massageLabel = new JLabel("Stock Exchange admin panel");

    JButton newUserButton = new JButton("Add new user");
    JButton closeStockButton = new JButton("Close Stock");
    JButton printAssetsButton = new JButton("Print Assets");

    StockInterface() {
        newUserButton.setBounds(BUTTON_VERTICAL_DISTANCE, BUTTON_HORIZONTAL_SPACING, BUTTON_WIDTH, BUTTON_HEIGHT);
        newUserButton.setFocusable(false);
        newUserButton.addActionListener(this);

        printAssetsButton.setBounds(BUTTON_VERTICAL_DISTANCE, (newUserButton.getY() + BUTTON_HORIZONTAL_SPACING), BUTTON_WIDTH, BUTTON_HEIGHT);
        printAssetsButton.setFocusable(false);
        printAssetsButton.addActionListener(this);

        closeStockButton.setBounds(BUTTON_VERTICAL_DISTANCE, (printAssetsButton.getY() + BUTTON_HORIZONTAL_SPACING), BUTTON_WIDTH, BUTTON_HEIGHT);
        closeStockButton.setFocusable(false);
        closeStockButton.addActionListener(this);

        massageLabel.setVerticalAlignment(JLabel.TOP);
        massageLabel.setHorizontalAlignment(JLabel.CENTER);
        massageLabel.setForeground(Color.WHITE);
        massageLabel.setFont(new Font("MV Boli", Font.PLAIN, 30));
        massageLabel.add(newUserButton);
        massageLabel.add(printAssetsButton);
        massageLabel.add(closeStockButton);

        frame.add(massageLabel);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.getContentPane().setBackground(Color.BLACK);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == newUserButton) {
            UserInterface NewUser = new UserInterface();
        } else if (e.getSource() == printAssetsButton) {
            StockPrintInterface PrintStock = new StockPrintInterface();
        } else if (e.getSource() == closeStockButton) {
            LogAdder.addLogInfo("Stock Closed by Admin");
            System.exit(0);
        }
    }
}
